# php-dropbox-sync

This application is used to sync files from DropBox with a local directory.

## config-example.json
Used as an example for creating a config.json file.  Please be sure to create 
a config.json based on the example file with your App Key and App Secret.

## ValidAccessToken.php
When you have setup your config.json file with your App's Key/Secret, run this 
file in the shell using the following command:

`php ValidAccessToken.php`

Follow the instructions printed on the screen, and it will automatically obtain 
and save an Access Token to the config.json file that can be used for future 
requests.

## SyncFiles.php
This file will need to be put within the directory you want to sync with DropBox.
Each time this file runs, it will sync all files and folders. To have this file
run periodically, a CRON job will need to be setup.